/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.spark.mllib.linalg.distributed

import java.util.Arrays

import breeze.linalg.{axpy => brzAxpy, inv, svd => brzSvd, DenseMatrix => BDM, DenseVector => BDV,
MatrixSingularException, SparseVector => BSV}
import breeze.numerics.{sqrt => brzSqrt}

import org.apache.spark.annotation.Since
import org.apache.spark.internal.Logging
import org.apache.spark.mllib.linalg._
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel

/**
* Created by grbulat on 19.12.16.
*/
class LeftSvdIndexedMatrix(@Since("1.0.0") override val rows: RDD[IndexedRow])
  extends IndexedRowMatrix(rows)
    with Logging {

  @Since("1.0.0")
  override def computeSVD(
                           k: Int,
                           computeU: Boolean = false,
                           rCond: Double = 1e-9)
  : SingularValueDecomposition[IndexedRowMatrix, Matrix] = {
    // maximum number of Arnoldi update iterations for invoking ARPACK
    val maxIter = math.max(300, k * 3)
    // numerical tolerance for invoking ARPACK
    val tol = 1e-10
    computeSVD(k, computeU, rCond, maxIter, tol, "dist-eigs")
  }

  /**
  * The actual SVD implementation, visible for testing.
  *
  * @param k        number of leading singular values to keep (0 &lt; k &lt;= n)
  * @param computeU whether to compute U
  * @param rCond    the reciprocal condition number
  * @param maxIter  max number of iterations (if ARPACK is used)
  * @param tol      termination tolerance (if ARPACK is used)
  * @param mode     computation mode (auto: determine automatically which mode to use,
  *                 local-svd: compute gram matrix and computes its full SVD locally,
  *                 local-eigs: compute gram matrix and computes its top eigenvalues locally,
  *                 dist-eigs: compute the top eigenvalues of the gram matrix distributively)
  * @return SingularValueDecomposition(U, s, V). U = null if computeU = false.
  */
  private[mllib] def computeSVD(
                                 k: Int,
                                 computeU: Boolean,
                                 rCond: Double,
                                 maxIter: Int,
                                 tol: Double,
                                 mode: String)
  : SingularValueDecomposition[IndexedRowMatrix, Matrix] = {
    val n = numCols().toInt
    require(k > 0 && k <= n, s"Requested k singular values but got k=$k and numCols=$n.")

    object SVDMode extends Enumeration {
      val LocalARPACK, LocalLAPACK, DistARPACK = Value
    }

    val computeMode = mode match {
      case "auto" =>
        if (k > 5000) {
          logWarning(s"computing svd with k=$k and n=$n, please check necessity")
        }

        // TODO: The conditions below are not fully tested.
        if (n < 100 || (k > n / 2 && n <= 15000)) {
          // If n is small or k is large compared with n, we better compute the Gramian matrix first
          // and then compute its eigenvalues locally, instead of making multiple passes.
          if (k < n / 3) {
            SVDMode.LocalARPACK
          } else {
            SVDMode.LocalLAPACK
          }
        } else {
          // If k is small compared with n, we use ARPACK with distributed multiplication.
          SVDMode.DistARPACK
        }
      case "local-svd" => SVDMode.LocalLAPACK
      case "local-eigs" => SVDMode.LocalARPACK
      case "dist-eigs" => SVDMode.DistARPACK
      case _ => throw new IllegalArgumentException(s"Do not support mode $mode.")
    }

    // Compute the eigen-decomposition of A' * A.
    val (sigmaSquares: BDV[Double], u: BDM[Double]) = computeMode match {
      case SVDMode.LocalARPACK =>
        require(k < n, s"k must be smaller than n in local-eigs mode but got k=$k and n=$n.")
        val G = computeGramianMatrix().asBreeze.asInstanceOf[BDM[Double]]
        EigenValueDecomposition.symmetricEigs(v => G * v, n, k, tol, maxIter)
      case SVDMode.LocalLAPACK =>
        // breeze (v0.10) svd latent constraint, 7 * n * n + 4 * n < Int.MaxValue
        require(n < 17515, s"$n exceeds the breeze svd capability")
        val G = computeGramianMatrix().asBreeze.asInstanceOf[BDM[Double]]
        val brzSvd.SVD(uFull: BDM[Double], sigmaSquaresFull: BDV[Double], _) = brzSvd(G)
        (sigmaSquaresFull, uFull)
      case SVDMode.DistARPACK =>
        if (rows.getStorageLevel == StorageLevel.NONE) {
          logWarning("The input data is not directly cached, which may hurt performance if its"
            + " parent RDDs are also uncached.")
        }
        require(k < n, s"k must be smaller than n in dist-eigs mode but got k=$k and n=$n.")
        EigenValueDecomposition.symmetricEigs(multiplyGramianMatrixBy, n, k, tol, maxIter)
    }

    val sigmas: BDV[Double] = brzSqrt(sigmaSquares)

    // Determine the effective rank.
    val sigma0 = sigmas(0)
    val threshold = rCond * sigma0
    var i = 0
    // sigmas might have a length smaller than k, if some Ritz values do not satisfy the convergence
    // criterion specified by tol after max number of iterations.
    // Thus use i < min(k, sigmas.length) instead of i < k.
    if (sigmas.length < k) {
      logWarning(s"Requested $k singular values but only found ${sigmas.length} converged.")
    }
    while (i < math.min(k, sigmas.length) && sigmas(i) >= threshold) {
      i += 1
    }
    val sk = i

    if (sk < k) {
      logWarning(s"Requested $k singular values but only found $sk nonzeros.")
    }

    // Warn at the end of the run as well, for increased visibility.
    if (computeMode == SVDMode.DistARPACK && rows.getStorageLevel == StorageLevel.NONE) {
      logWarning("The input data was not directly cached, which may hurt performance if its"
        + " parent RDDs are also uncached.")
    }

    val s = Vectors.dense(Arrays.copyOfRange(sigmas.data, 0, sk))
    val rowCount = this.numRows().toInt
    val V = Matrices.dense(rowCount, sk, Arrays.copyOfRange(u.data, 0, rowCount * sk))

    if (computeU) {
      // N = Vk * Sk^{-1}
      val N = new BDM[Double](rowCount, sk, Arrays.copyOfRange(u.data, 0, rowCount * sk))
      var i = 0
      var j = 0
      while (j < sk) {
        i = 0
        val sigma = sigmas(j)
        while (i < rowCount) {
          N(i, j) /= sigma
          i += 1
        }
        j += 1
      }
      val mat = this.toBreeze().t * N
      val indexedRows = rows.map(_.index).map { case i =>
        IndexedRow(i, Vectors.dense(mat(i.toInt to i.toInt, 0 to mat.cols - 1).toArray))
      }
      val U = new IndexedRowMatrix(indexedRows, rowCount, numCols.toInt)
      SingularValueDecomposition(U, s, V)
    } else {
      SingularValueDecomposition(null, s, V)
    }
  }

  /**
  * Multiplies the Gramian matrix `A A^T` by a dense vector on the right without computing `A A^T`.
  *
  * @param v a dense vector whose length must match the number of columns of this matrix
  * @return a dense vector representing the product
  */
  private[mllib] def multiplyGramianMatrixBy(v: BDV[Double]): BDV[Double] = {
    val n = numCols().toInt
    val vbr = rows.context.broadcast(v)
    val initZeroVector = BDV.zeros[Double](n)
    // B = A(Transposed) * v = matrixRow(A, i) * vectorComponent(v, i)
    val aTMultWithVec = rows.treeAggregate(initZeroVector)(
      seqOp = (U, r) => {
        val rBrz = r.vector.asBreeze
        val a = rBrz * vbr.value.valueAt(r.index.toInt)
        U + a
      }, combOp = (U1, U2) => U1 + U2)
    // A * B
    rows.treeAggregate(BDV.zeros[Double](n))(
      (interResult: BDV[Double], row: IndexedRow) => {
        val resVectorComponent = row.vector.asBreeze.dot(aTMultWithVec)
        interResult.update(row.index.toInt, resVectorComponent)
        interResult
      },
      (leftVector: BDV[Double], rightVector: BDV[Double]) =>
        leftVector + rightVector
    )
  }
}
